(function () {
	Plugin.register('anim_utils', {
    title: 'Anim Utils',
    author: 'JakeSmythUK',
    icon: 'fa-cogs',
    description: 'Some animation functions.',
    version: '1.0.0',
    variant: 'both',
    onload() {
		
		/** TODO LIST
		 * 
		 */
		
		mainButton = new Action('anim_utils_main_window_btn', {
			name: 'Anim Utils',
			description: 'Open Anim Utils Window.',
			icon: 'fa-cogs',
			click: function () {
				let dialog = new Dialog({
					id: 'anim_utils_main_window',
					title: 'Animation Utilities',
					lines: [
						'<button onclick="MultiplyAnimSpeed();" style="width: 100%; margin-top: 10px;">Multiply Speed</button>',
						'<button onclick="MultiplyAnimLength();" style="width: 100%; margin-top: 20px;">Multiply Length</button>',
						'<button style="width: 100%; margin-top: 20px;">TEST</button>'
					]
				}).show();
			}
        });
        MenuBar.addAction(mainButton, 'animation.-1');
    },
    onunload() {
        mainButton.delete();
    }
});
})();

function MultiplyAnimSpeed() {
	if (Animation.selected){
		let dialog = new Dialog({
			id: 'anim_utils_change_anim_speed',
			title: 'Multiply Animation Speed',
			form: {
				multiplier: {label: 'Multiplier', type: 'number', value: 1, min: 0, max: 10}
			},
			onConfirm: function(formData) {
				
				var multiplier = formData.multiplier;
				
				var beforeAll = []
				
				for (var i = 0; i < Group.all.length; i++) {
					beforeAll.push(Group.all[i].selected);
					Group.all[i].selected = true;
					if (Group.all[i]) {
						if (Group.all[i]) {
							Group.all[i].select();
						}
					}
				}
				
				unselectAll();
				selectAll();
				updateSelection();
				
				Undo.initEdit({keyframes: Timeline.selected});
				
				var values_changed = false; 
				for (var kf of Timeline.selected) {
					kf.time_before = kf.time;
					let old_time = kf.time;
					kf.time = old_time * multiplier;
					
					if (old_time != kf.time)
						values_changed = true;
				}
				
				var deleted = []
				for (var kf of Timeline.selected) {
					delete kf.time_before;
					kf.replaceOthers(deleted);
				}
				Blockbench.setStatusBarText();
				if (values_changed) {
					Undo.addKeyframeCasualties(deleted);
					Undo.finishEdit('Changed animation speed');
				} else {
					Undo.cancelEdit();
				}
				
				unselectAll();
				updateSelection();
				
				for (var i = 0; i < Group.all.length; i++) {
					Group.all[i].selected = beforeAll[i];
					if (Group.all[i] && beforeAll[i]) 
						Group.all[i].select();
				}
				
				updateSelection();
				
				this.hide()
			}
		}).show();
	} else {
		Blockbench.showQuickMessage('message.no_animation_selected');
	}
}

function MultiplyAnimLength() {
	if (Animation.selected){
		let dialog = new Dialog({
			id: 'anim_utils_change_anim_length',
			title: 'Multiply Animation Length',
			form: {
				multiplier: {label: 'Multiplier', type: 'number', value: 1, min: 0, max: 10}
			},
			onConfirm: function(formData) {
				
				var multiplier = formData.multiplier;
				
				Undo.initEdit({animations: [Animation.selected]});
				
				let time = Animation.selected.length;
				
				Animation.selected.setLength(time * multiplier);
				Timeline.revealTime(time * multiplier);
				
				Undo.finishEdit('Changed animation length');
				
				this.hide()
			}
		}).show();
	} else {
		Blockbench.showQuickMessage('message.no_animation_selected');
	}
}